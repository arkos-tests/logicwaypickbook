﻿using System;

using UnityEngine;
using UnityEngine.UI;

using Zenject;

public class PictureItemPlaceholderMB : MonoBehaviour
    , IPoolable<PictureItemPlaceholderMB.InitData, IMemoryPool>, IDisposable
{
    [SerializeField] private Image _image;


    private IMemoryPool _pool;


    public void Set()
    {
        _image.color = Color.white;
    }



    void IPoolable<InitData, IMemoryPool>.OnSpawned(InitData data, IMemoryPool pool)
    {
        transform.SetParent(data.Parent);

        _image.transform.localPosition = data.ItemData.Position;

        _image.color = new Color(1f, 1f, 1f, 0.25f);
        _image.sprite = data.ItemData.Sprite;
        name = _image.sprite.name;
        (_image.transform as RectTransform).sizeDelta = _image.sprite.bounds.size * _image.sprite.pixelsPerUnit;
    }

    void IDisposable.Dispose()
    {
        _pool.Despawn(this);
    }

    void IPoolable<InitData, IMemoryPool>.OnDespawned()
    {
        _pool = null;
    }


    public class Factory : PlaceholderFactory<InitData, PictureItemPlaceholderMB>
    {
    }


    public class InitData
    {
        public RectTransform Parent;
        public PictureConfigSO.PictureItemData ItemData;


        public InitData(RectTransform parent, PictureConfigSO.PictureItemData itemData)
        {
            Parent = parent;
            ItemData = itemData;
        }
    }
}