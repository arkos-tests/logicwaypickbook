﻿using System;

using UnityEngine;
using UnityEngine.UI;

public class ReButtonMB : MonoBehaviour
{
    [SerializeField] private Button _button;


    private ProgressManager _progressManager;


    private void OnEnable()
    {
        /* CRUTCH */
        Init(null);
        /* CRUTCH */
        _button.onClick.AddListener(Click);
    }

    private void Click()
    {
        _progressManager.Reset(StaticContainer.Picture);
        Scenes.Core.LoadScene();
    }

    public void Init(ProgressManager progressManager)
    {
        _progressManager = progressManager;
        /* CRUTCH */
        if (_progressManager == null)
            _progressManager = new ProgressManager();
        /* CRUTCH */
    }
}
