﻿using JetBrains.Annotations;

using UnityEngine;
using UnityEngine.UI;

public class PicturePreviewMB : MonoBehaviour
{
    [SerializeField] private bool _selfInit;
    [SerializeField] private int _skip = 9;
    [SerializeField] private float _targetSize = 1023f;


#if UNITY_EDITOR
    [SerializeField, CanBeNull]
#endif // UNITY_EDITOR
    private PictureConfigSO _pictureConfig;
    private ProgressManager _progressManager;


    private void OnEnable()
    {
        if (_selfInit)
        {
            Init(StaticContainer.Picture);
        }
    }


    private float GetScale()
        => (transform as RectTransform).rect.width / _targetSize;


    public void Init(PictureConfigSO pictureConfig)
    {
        _pictureConfig = pictureConfig;
        _progressManager = new ProgressManager();
        Spawn();
    }


    [ContextMenu(nameof(Spawn))]
    private void Spawn()
    {
        if (!_pictureConfig)
            return;

        Clear();

        var scale = GetScale();
        var progress = _progressManager.GetProgress(_pictureConfig);
        var items = _pictureConfig.Items;
        for (int i = 0; i < items.Count; i++)
        {
            var item = items[i];
            var go = new GameObject();
            go.transform.SetParent(transform);
            var rtf = go.AddComponent<RectTransform>();

            rtf.localPosition = item.Position * scale;

            var image = go.AddComponent<Image>();
            image.sprite = item.Sprite;
            rtf.sizeDelta = image.sprite.bounds.size * image.sprite.pixelsPerUnit;
            rtf.localScale = Vector3.one * scale;
            go.name = image.sprite.name;
            image.color = progress.Contains(i) ? Color.white : new Color(1f, 1f, 1f, 0.25f);
        }
    }


    [ContextMenu(nameof(Clear))]
    public void Clear()
    {
        while (transform.childCount > _skip)
        {
            DestroyImmediate(transform.GetChild(_skip).gameObject);
        }
    }


#if UNITY_EDITOR
    [ContextMenu(nameof(Bake))]
    private void Bake()
    {
        if (!_pictureConfig)
            return;

        _pictureConfig.Items.Clear();
        var scale = GetScale();

        var i = 0;
        foreach (Transform child in transform)
        {
            i++;
            if (i < _skip)
                continue;

            var image = child.GetComponent<Image>();
            if (!image)
                continue;

            var sprite = image.sprite;
            if (!sprite)
                continue;

            _pictureConfig.Items.Add(new PictureConfigSO.PictureItemData()
            { Sprite = sprite, Position = child.localPosition / scale });
        }
    }
#endif // UNITY_EDITOR
}
