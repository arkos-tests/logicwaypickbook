﻿using System;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using Zenject;

public class PictureItemMB : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    , IPoolable<PictureItemMB.InitData, IMemoryPool>, IDisposable
{
    [SerializeField] private Image _image;


    private ProgressManager _progressManager;
    // init
    private IMemoryPool _pool;
    private ScrollRect _scrollRect;
    private PictureItemPlaceholderMB _placeholder;
    private int _index;
    private PictureConfigSO.PictureItemData _itemData;
    // pick
    private bool _picked;
    private Vector2 _pickOffset;
    // finish
    private bool _set;


    [Inject]
    private void Inject(ProgressManager progressManager)
    {
        _progressManager = progressManager;
    }


    private void Update()
    {
        if (!_picked)
            return;

        _image.transform.position = _pickOffset + (Vector2)Input.mousePosition;
    }


    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        if (_set)
            return;

        _pickOffset = (Vector2)transform.position - eventData.position;
        _picked = true;
        _image.maskable = false;
        _scrollRect.enabled = false;
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        if (_set)
            return;

        _picked = false;
        _image.maskable = true;
        _scrollRect.enabled = true;
        _image.transform.localPosition = Vector2.zero;

        var distance = eventData.position + _pickOffset - (Vector2)_placeholder.transform.position;
        distance = new Vector2(Mathf.Abs(distance.x), Mathf.Abs(distance.y));
        var minDistance = _itemData.GetDistance();
        if (distance.x >= minDistance.x || distance.y >= minDistance.y)
        {
            return;
        }

        _set = true;
        _placeholder.Set();
        gameObject.SetActive(false);
        _progressManager.OnItemSet(_index, StaticContainer.Picture);
    }

    void IPoolable<InitData, IMemoryPool>.OnSpawned(InitData data, IMemoryPool pool)
    {
        _pool = pool;
        _index = data.Index;
        _set = false;
        _scrollRect = data.ScrollRect;
        _placeholder = data.Placeholder;
        _itemData = data.ItemData;

        transform.SetParent(data.Parent);

        _image.sprite = _itemData.Sprite;
        name = _image.sprite.name;
        (_image.transform as RectTransform).sizeDelta = _image.sprite.bounds.size * _image.sprite.pixelsPerUnit;
        gameObject.SetActive(true);
    }

    void IDisposable.Dispose()
    {
        _pool.Despawn(this);
    }

    void IPoolable<InitData, IMemoryPool>.OnDespawned()
    {
        _pool = null;
    }


    public class Factory : PlaceholderFactory<InitData, PictureItemMB>
    {
    }


    public class InitData
    {
        public RectTransform Parent;
        public ScrollRect ScrollRect;
        public PictureItemPlaceholderMB Placeholder;
        public int Index;
        public PictureConfigSO.PictureItemData ItemData;


        public InitData(RectTransform parent,
            ScrollRect scrollRect, PictureItemPlaceholderMB placeholder,
            int index, PictureConfigSO.PictureItemData itemData)
        {
            Parent = parent;
            ScrollRect = scrollRect;
            Placeholder = placeholder;
            Index = index;
            ItemData = itemData;
        }
    }
}