﻿using System.Collections.Generic;

using UnityEngine;

public interface IProgressManager
{
    List<int> GetProgress(PictureConfigSO pictureConfig);
    void OnItemSet(int index, PictureConfigSO pictureConfig);
    void Reset(PictureConfigSO pictureConfig);
}

/* TODO:
 * refactor with separate progress entity
 * contains: progress, baked preview
 */
public class ProgressManager : IProgressManager
{
    private Dictionary<int, List<int>> _progressCache = new Dictionary<int, List<int>>();
    public List<int> GetProgress(PictureConfigSO pictureConfig)
    {
        if (!pictureConfig)
            return null;

        var hash = pictureConfig.name.GetHashCode();
        if (_progressCache.ContainsKey(hash))
            return _progressCache[hash];

        var list = new List<int>();
        _progressCache.Add(hash, list);
        var progressValue = PlayerPrefs.GetInt(pictureConfig.name, 0);
        var max = Mathf.Log(progressValue, 2);
        for (int i = 0; i <= max; i++)
        {
            var pow = Mathf.RoundToInt(Mathf.Pow(2, i));
            if ((progressValue & pow) == pow)
                list.Add(i);
        }
        return list;
    }


    public void OnItemSet(int index, PictureConfigSO pictureConfig)
    {
        var progress = GetProgress(pictureConfig);
        progress.Add(index);

        var progressValue = 0;
        foreach (int progressIndex in progress)
        {
            progressValue += Mathf.RoundToInt(Mathf.Pow(2, progressIndex));
        }
        PlayerPrefs.SetInt(pictureConfig.name, progressValue);
        PlayerPrefs.Save();

        if (progress.Count >= pictureConfig.Items.Count)
        {
            Scenes.Final.LoadScene();
        }
    }

    public void Reset(PictureConfigSO pictureConfig)
    {
        /* CRUTCH */
        if (!pictureConfig)
        {
            PlayerPrefs.DeleteAll();
            return;
        }
        /* CRUTCH */

        PlayerPrefs.SetInt(pictureConfig.name, 0);
        PlayerPrefs.Save();
    }
}
