﻿using System;
using System.Collections.Generic;

using UnityEngine;

[CreateAssetMenu(fileName = nameof(PictureConfigSO), menuName = nameof(PictureConfigSO))]
public class PictureConfigSO : ScriptableObject
{
    public const int MinSize = 50;


    public List<PictureItemData> Items;


    [Serializable]
    public class PictureItemData
    {
        public Vector2 Position;
        public Sprite Sprite;

        public Vector2 GetDistance()
        {
            var halfsize = Sprite.bounds.size / 2f;
            return new Vector2(
                Mathf.Max(halfsize.x, PictureConfigSO.MinSize),
                Mathf.Max(halfsize.y, PictureConfigSO.MinSize));
        }
    }
}
