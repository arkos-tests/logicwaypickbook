﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class LoadSceneButtonMB : MonoBehaviour
{
    [SerializeField] private Button _button;
    [SerializeField] private Scenes _scene;


    private void OnEnable()
    {
        _button.onClick.AddListener(Click);
    }

    private void OnDisable()
    {
        _button.onClick.RemoveListener(Click);
    }


    private void Click()
    {
        _scene.LoadScene();
    }


#if UNITY_EDITOR
    private void OnValidate()
    {
        if (!_button)
            _button = GetComponent<Button>();
    }
#endif // UNITY_EDITOR
}
