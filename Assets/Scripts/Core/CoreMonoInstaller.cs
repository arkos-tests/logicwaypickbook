using UnityEngine;

using Zenject;

public class CoreMonoInstaller : MonoInstaller
{
    [SerializeField] private PictureItemsMB _pictureItems;
    [Header("Prefabs")]
    [SerializeField] private PictureItemMB _itemPrefab;
    [SerializeField] private PictureItemPlaceholderMB _placeholderPrefab;


    public override void InstallBindings()
    {
        if (!Container.HasBinding<IProgressManager>())
        {
            Container.BindInterfacesAndSelfTo<ProgressManager>()
                //.FromInstance(StaticContainer.ProgressManager)
                .AsSingle();
        }

        Container.BindInstance(_pictureItems).AsSingle();

        Container.BindFactory<PictureItemMB.InitData, PictureItemMB, PictureItemMB.Factory>()
            .FromPoolableMemoryPool<PictureItemMB.InitData, PictureItemMB, PictureItemMBPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(_itemPrefab));

        Container.BindFactory<PictureItemPlaceholderMB.InitData, PictureItemPlaceholderMB, PictureItemPlaceholderMB.Factory>()
            .FromPoolableMemoryPool<PictureItemPlaceholderMB.InitData, PictureItemPlaceholderMB, PictureItemPlaceholderMBPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(_placeholderPrefab));
    }


    private class PictureItemMBPool : MonoPoolableMemoryPool<PictureItemMB.InitData, IMemoryPool, PictureItemMB>
    {
    }

    private class PictureItemPlaceholderMBPool : MonoPoolableMemoryPool<PictureItemPlaceholderMB.InitData, IMemoryPool, PictureItemPlaceholderMB>
    {
    }
}