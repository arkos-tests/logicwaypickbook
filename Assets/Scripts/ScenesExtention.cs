﻿using UnityEngine.SceneManagement;

public static class ScenesExtention
{
    public static void LoadScene(this Scenes scene)
        => SceneManager.LoadScene(scene.ToString());
}