﻿using System;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Zenject;

public class FeedItemMB : MonoBehaviour
    , IPoolable<FeedItemMB.InitData, IMemoryPool>, IDisposable
{
    [SerializeField] private PicturePreviewMB _frame;
    [SerializeField] private Button _button;
    [SerializeField] private GameObject _lock;


    private IMemoryPool _pool;
    private PictureConfigSO _picture;


    private void OnEnable()
    {
        if (_picture)
            _button.onClick.AddListener(GoDraw);
        else
            _button.onClick.RemoveListener(GoDraw);
    }

    private void OnDisable()
    {
        _button.onClick.RemoveListener(GoDraw);
    }


    private void GoDraw()
    {
        StaticContainer.Picture = _picture;
        Scenes.Core.LoadScene();
        GC.Collect();
        Resources.UnloadUnusedAssets();
    }


    void IPoolable<InitData, IMemoryPool>.OnSpawned(InitData data, IMemoryPool pool)
    {
        transform.SetParent(data.Parent);

        _picture = data.PictureConfig;
        OnEnable();

        _lock.SetActive(!_picture);
        if (!_picture)
        {
            _frame.Clear();
            return;
        }

        _frame.Init(_picture);
    }

    void IDisposable.Dispose()
    {
        _pool.Despawn(this);
    }

    void IPoolable<InitData, IMemoryPool>.OnDespawned()
    {
        _pool = null;
    }


    public class Factory : PlaceholderFactory<InitData, FeedItemMB>
    {
    }


    public class InitData
    {
        public RectTransform Parent;
        public PictureConfigSO PictureConfig;


        public InitData(RectTransform parent, PictureConfigSO pictureConfig)
        {
            Parent = parent;
            PictureConfig = pictureConfig;
        }
    }


#if UNITY_EDITOR
    private void OnValidate()
    {
        if (!_button)
            _button = GetComponent<Button>();
    }
#endif // UNITY_EDITOR
}
