﻿using UnityEngine;

using Zenject;

public class FeedItemsMB : MonoBehaviour
{
    [SerializeField] private RectTransform _root;
    [SerializeField] private int _minCount;


    private FeedItemMB.Factory _itemsFactory;


    [Inject]
    private void Inject(FeedItemMB.Factory itemsFactory)
    {
        _itemsFactory = itemsFactory;
    }


    private void OnEnable()
    {
        foreach (Transform child in _root)
        {
            Destroy(child.gameObject);
        }

        var pictures = Resources.LoadAll<PictureConfigSO>("");
        var count = Mathf.Max(_minCount, pictures.Length);
        for (int i = 0; i < count; i++)
        {
            _itemsFactory.Create(new FeedItemMB.InitData(_root, i < pictures.Length ? pictures[i] : null));
        }
    }
}
