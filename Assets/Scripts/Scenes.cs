﻿public enum Scenes
{
    Loading     = 0,
    Feed        = 1,
    Core        = 2,
    Final       = 3,
}
