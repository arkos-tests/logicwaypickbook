using UnityEngine;

using Zenject;

public class FeedMonoInstaller : MonoInstaller
{
    [SerializeField] private FeedItemsMB _feedItems;
    [Header("Prefabs")]
    [SerializeField] private FeedItemMB _feedItemPrefab;
    [SerializeField] private PictureItemPlaceholderMB _picturePlaceholderPrefab;


    public override void InstallBindings()
    {
        if (!Container.HasBinding<IProgressManager>())
        {
            Container.BindInterfacesAndSelfTo<ProgressManager>()
                //.FromInstance(StaticContainer.ProgressManager)
                .AsSingle();
        }

        if (!Container.HasBinding<FeedItemsMB>())
            Container.BindInterfacesAndSelfTo<FeedItemsMB>().FromInstance(_feedItems).AsSingle();

        Container.BindFactory<FeedItemMB.InitData, FeedItemMB, FeedItemMB.Factory>()
            .FromPoolableMemoryPool<FeedItemMB.InitData, FeedItemMB, FeedItemMBPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(_feedItemPrefab));

        Container.BindFactory<PictureItemPlaceholderMB.InitData, PictureItemPlaceholderMB, PictureItemPlaceholderMB.Factory>()
            .FromPoolableMemoryPool<PictureItemPlaceholderMB.InitData, PictureItemPlaceholderMB, PictureItemPlaceholderMBPool>(poolBinder => poolBinder
                .WithInitialSize(5)
                .FromComponentInNewPrefab(_picturePlaceholderPrefab));
    }


    private class FeedItemMBPool : MonoPoolableMemoryPool<FeedItemMB.InitData, IMemoryPool, FeedItemMB>
    {
    }

    private class PictureItemPlaceholderMBPool : MonoPoolableMemoryPool<PictureItemPlaceholderMB.InitData, IMemoryPool, PictureItemPlaceholderMB>
    {
    }
}