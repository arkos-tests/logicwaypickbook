﻿using UnityEngine;
using UnityEngine.UI;

using Zenject;

public class PictureItemsMB : MonoBehaviour
{
    [SerializeField] private ScrollRect _itemsScroll;
    [SerializeField] private RectTransform _canvasRoot;


    // Inject
    private ProgressManager _progressManager;
    private PictureItemMB.Factory _itemFactory;
    private PictureItemPlaceholderMB.Factory _placeholderFactory;


    [Inject]
    private void Inject(
        ProgressManager progressManager,
        PictureItemMB.Factory itemFactory,
        PictureItemPlaceholderMB.Factory placeholderFactory)
    {
        _progressManager = progressManager;
        _itemFactory = itemFactory;
        _placeholderFactory = placeholderFactory;
    }


    private void Awake()
    {
        var pictureConfig = StaticContainer.Picture;
        var items = pictureConfig.Items;
        var progress = _progressManager.GetProgress(pictureConfig);
        if (progress.Count >= items.Count)
        {
            Scenes.Final.LoadScene();
            return;
        }

        foreach (Transform child in _itemsScroll.content)
        {
            Destroy(child.gameObject);
        }

        foreach (Transform child in _canvasRoot)
        {
            Destroy(child.gameObject);
        }

        for (int index = 0; index < items.Count; index++)
        {
            // Data
            var itemData = items[index];

            // Placeholder
            var placeholder = _placeholderFactory.Create(new PictureItemPlaceholderMB.InitData(_canvasRoot, itemData));
            if (progress.Contains(index))
            {
                placeholder.Set();
                continue;
            }

            // Item
            _itemFactory.Create(new PictureItemMB.InitData(_itemsScroll.content, _itemsScroll, placeholder, index, itemData));
        }
    }
}
